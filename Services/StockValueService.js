"use strict";
exports.__esModule = true;
var StockValue_1 = require("../Models/StockValue");
var StockValueService = /** @class */ (function () {
    function StockValueService() {
    }
    StockValueService.prototype.create = function (symbol, price, callback) {
        var stockValue = new StockValue_1.StockValue();
        stockValue.setSymbol(symbol);
        stockValue.setPrice(price);
        stockValue.create(callback);
    };
    StockValueService.prototype.browse = function (request, response, callback) {
        var stockValue = new StockValue_1.StockValue();
        stockValue.filterQuery(request, callback);
    };
    return StockValueService;
}());
exports.StockValueService = StockValueService;
