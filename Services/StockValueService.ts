import {StockValue} from '../Models/StockValue';

export class StockValueService{

    create(
        symbol:string,
        price:number,
        callback
    ):void{
        let stockValue:StockValue = new StockValue();

        stockValue.setSymbol(symbol);
        stockValue.setPrice(price);

        stockValue.create(callback);
    }

    browse(request, response, callback){

        let stockValue:StockValue = new StockValue();
        stockValue.filterQuery(request, callback);

    }

}