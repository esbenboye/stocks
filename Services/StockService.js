"use strict";
exports.__esModule = true;
var Stock_1 = require("../Models/Stock");
var StockService = /** @class */ (function () {
    function StockService() {
    }
    StockService.prototype.getAll = function (callback) {
        var stock = new Stock_1.Stock();
        stock.getAll(callback);
    };
    StockService.prototype.find = function (symbol, callback) {
        var stock = new Stock_1.Stock();
        stock.find(symbol, callback);
    };
    StockService.prototype.create = function (symbol, sector, securityType, callback) {
        var stock = new Stock_1.Stock();
        stock.setSymbol(symbol);
        stock.setSector(sector);
        stock.setSecurityType(securityType);
        stock.save(callback);
    };
    StockService.prototype.update = function (symbol, sector, securityType, callback) {
        var stock = new Stock_1.Stock();
        stock.setSymbol(symbol);
        stock.setSector(sector);
        stock.setSecurityType(securityType);
        stock.save(callback);
    };
    return StockService;
}());
exports.StockService = StockService;
