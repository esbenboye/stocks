import {Stock} from '../Models/Stock';

export class StockService{

    getAll(callback){
        let stock:Stock = new Stock();
        stock.getAll(callback);
    }


    find(symbol:string, callback){
        let stock:Stock = new Stock();
        stock.find(symbol, callback);
    }

    create(symbol:string, sector:string, securityType:string, callback){
        let stock:Stock = new Stock();
        stock.setSymbol(symbol);
        stock.setSector(sector);
        stock.setSecurityType(securityType);

        stock.save(callback);
    }

    update(symbol:string, sector:string, securityType:string, callback){
        let stock:Stock = new Stock();
        stock.setSymbol(symbol);
        stock.setSector(sector);
        stock.setSecurityType(securityType);
        stock.save(callback);
    }
}