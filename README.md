Stocks
======
A little project I put together for a (possible) job interview.

Requirements:
--------
- Docker
- NPM
- Node

How to run the project:
--------
1. Start by running the provided Docker image for a mysql database `docker-compose up`
2. Run the sql files found in `Sql/`
3. Install node modules `npm install`
4. Start the http server `node Servers/HttpServer.js` 
5. Start the socket server `node Servers/SocketServer.js`

Todo:
--------
 - Create Docker image for NodeJS
 - Make migrate tool to create tables
 - Make DB seed with some sample data
 - Add error handling (currently none)
 - Add input validation (currently none)
 - Refactor to not have hardcoded values in DB connection
 - Add endpoint for gainers and losers

Endpoints (See Postman collection in project root):
--------

`POST /stockvalue` - Emulates a call to the socket server. All fields should be filled.

`GET /stockvalue/browse` - Browse/search stockvalues. More details under "Browsing"

Browsing (See Postman collection in project root)
--------
Use GET query params to search/filter the stockvalues:

`GET /stockvalue/browser/?[column]__[comparitor]=[value]&order=[column]:[direction]`

Params `id__eq=10` will return the stockvalues where `id` is equal to 10

Params `price__gt=1` will return the stockvalues where `price` is greater than 1

Params `created_at__gt=2018-01-01` will return the stockvalues created later than 2018-01-01

Params `order=id:asc` will order by `id`, `ascending`

Params `order=created_at:asc` will order by `id`, `ascending`

The following comparators are available:

`gt` - Greater than

`gte` - Greater than or equal to

`lt` - Less than

`lte` - Less than or equal to

`eq` - Equals

All columns are currently available for all comparisons and it is possible to use multiple conditions (e.g. `?symbol__eq=SNAP&id__gt=10`)

CheatSheet, development:
--------
1. Listen for change while developing `node_modules/nodemon/bin/nodemin.js`
2. Automatically compile .ts files `node_modules/typescript/bin/tsc -w *.ts`

