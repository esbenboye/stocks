"use strict";
exports.__esModule = true;
var Connection_1 = require("./Connection");
var QueryBuilder_1 = require("./QueryBuilder");
var StockValue = /** @class */ (function () {
    function StockValue() {
        this.table_name = "stock_values";
        this.connection = new Connection_1.Connection();
    }
    StockValue.prototype.setPrice = function (price) {
        this.price = price;
    };
    StockValue.prototype.setSymbol = function (symbol) {
        this.symbol = symbol;
    };
    StockValue.prototype.setBidPrice = function (bidPrice) {
        this.bidPrice = bidPrice;
    };
    StockValue.prototype.setBidSize = function (bidSize) {
        this.bidSize = bidSize;
    };
    StockValue.prototype.setAskSize = function (askSize) {
        this.askSize = askSize;
    };
    StockValue.prototype.setLastUpdated = function (lastUpdated) {
        this.lastUpdated = lastUpdated;
    };
    StockValue.prototype.setLastSalePrice = function (lastSalePrice) {
        this.lastSalePrice = lastSalePrice;
    };
    StockValue.prototype.setLastSaleTime = function (lastSaleTime) {
        this.lastSaleTime = lastSaleTime;
    };
    StockValue.prototype.setVolume = function (volume) {
        this.volume = volume;
    };
    StockValue.prototype.setMarketPercent = function (marketPercent) {
        this.marketPercent = marketPercent;
    };
    StockValue.prototype.setSeq = function (seq) {
        this.seq = seq;
    };
    StockValue.prototype.create = function (callback) {
        var sql = "\n            INSERT INTO `" + this.table_name + "` \n                (\n                    `symbol`,\n                    `price`\n                ) \n            VALUES(\n                    '" + this.symbol + "',\n                    " + this.price + "\n                );";
        this.connection.runQuery(sql, callback);
    };
    StockValue.prototype.filterQuery = function (request, callback) {
        var queryBuilder = new QueryBuilder_1.QueryBuilder();
        queryBuilder.from('stock_values');
        if (request.query.order) {
            var split = request.query.order.split(":");
            queryBuilder.orderBy(split[0], split[1]);
        }
        for (var key in request.query) {
            var value = request.query[key];
            if (key.indexOf('__') > 0) {
                var split = key.split("__");
                var column = split[0];
                var txtComparator = split[1];
                queryBuilder.where(column, txtComparator, value);
            }
        }
        var sql = queryBuilder.getSelect();
        this.connection.runQuery(sql, callback);
    };
    return StockValue;
}());
exports.StockValue = StockValue;
