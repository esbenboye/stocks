"use strict";
exports.__esModule = true;
var mysql = require("mysql");
var Connection = /** @class */ (function () {
    function Connection() {
        this.pool = mysql.createPool({
            host: '127.0.0.1',
            port: 9002,
            user: 'stocks',
            password: 'stock1234',
            database: 'stocks'
        });
    }
    Connection.prototype.runQuery = function (query, callback) {
        this.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
            }
            else {
                conn.query(query, callback);
            }
        });
    };
    Connection.prototype.getConnection = function (callback) {
        this.pool.getConnection(callback);
    };
    return Connection;
}());
exports.Connection = Connection;
