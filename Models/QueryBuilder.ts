export class QueryBuilder{
    private table:string;
    private conditions:Array<string> = [];
    private orderByColumn:string;
    private orderDirection:string;
    private columns:Array<string> = [];

    private comparators:object = {
        'gt':'>',
        'gte':'>=',
        'lt':'<',
        'lte':'<=',
        'eq':'='
    }

    select(cols:Array<String>){
        this.columns = [];

        for(let index in cols){
            let col = cols[index];
            this.columns.push(`\`${col}\``);
        }

    }

    from(table:string){
        this.table = table;
    }

    where(column, comparator, value){
        let actualComparator:string = this.comparators[comparator];
        let whereString = ` \`${column}\` ${actualComparator} '${value}'`;
        this.conditions.push(whereString);
    }

    orderBy(column:string, direction:string){
        this.orderByColumn = column;
        this.orderDirection = direction;
    }

    getSelect():string{
        let cols = this.columns.join(',');
        let query:Array<String> = [];
        query.push(`SELECT * FROM \`${this.table}\``);
        if(this.conditions.length > 0){
            query.push("WHERE");
            this.conditions.forEach( value => query.push(value) );
        }

        if(this.orderByColumn && this.orderDirection){
            query.push(`ORDER BY ${this.orderByColumn} ${this.orderDirection}`);
        }

        return query.join(" ");
    }

    getDelete():string{
        return "DELETE FROM...";
    }

    getUpdate(values):string{
        return "UPDATE ... SET x=x, y=y ..";
    }



}