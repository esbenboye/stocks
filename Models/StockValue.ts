import {Connection} from './Connection';
import {QueryBuilder} from './QueryBuilder';

export class StockValue{

    private symbol:string;
    private bidPrice:number;
    private bidSize:number;
    private askSize:number;
    private lastUpdated:number;
    private lastSalePrice:number;
    private lastSaleTime:number;
    private volume:number;
    private marketPercent:number;
    private seq:number;

    private price:number;

    private connection:Connection;

    private table_name = "stock_values";

    constructor(){
        this.connection = new Connection();
    }

    setPrice(price:number){
        this.price = price;
    }

    setSymbol(symbol: string): void{
        this.symbol = symbol;
    }

    setBidPrice(bidPrice: number): void{
        this.bidPrice = bidPrice;
    }

    setBidSize(bidSize: number): void{
        this.bidSize = bidSize;
    }

    setAskSize(askSize: number): void{
        this.askSize = askSize;
    }

    setLastUpdated(lastUpdated:number): void{
        this.lastUpdated = lastUpdated;
    }

    setLastSalePrice(lastSalePrice: number): void{
        this.lastSalePrice = lastSalePrice;
    }

    setLastSaleTime(lastSaleTime: number): void{
        this.lastSaleTime = lastSaleTime;
    }

    setVolume(volume: number): void{
        this.volume = volume;
    }

    setMarketPercent(marketPercent: number): void{
        this.marketPercent = marketPercent;
    }

    setSeq(seq:number):void{
        this.seq = seq;
    }

    create(callback): void{

        let sql:string = `
            INSERT INTO \`${this.table_name}\` 
                (
                    \`symbol\`,
                    \`price\`
                ) 
            VALUES(
                    '${this.symbol}',
                    ${this.price}
                );`;
        this.connection.runQuery(sql, callback);
    }

    filterQuery(request, callback){
        let queryBuilder:QueryBuilder = new QueryBuilder();

        queryBuilder.from('stock_values');

        if(request.query.order){
            let split = request.query.order.split(":");
            queryBuilder.orderBy(split[0], split[1]);
        }

        for(let key in request.query){
            let value = request.query[key];
            if(key.indexOf('__') > 0){
                let split = key.split("__");
                let column = split[0];
                let txtComparator = split[1];
                queryBuilder.where(column, txtComparator, value);
            }
        }

        let sql:string = queryBuilder.getSelect();
        this.connection.runQuery(sql, callback);
    }

}

