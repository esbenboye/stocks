"use strict";
exports.__esModule = true;
var QueryBuilder = /** @class */ (function () {
    function QueryBuilder() {
        this.conditions = [];
        this.columns = [];
        this.comparators = {
            'gt': '>',
            'gte': '>=',
            'lt': '<',
            'lte': '<=',
            'eq': '='
        };
    }
    QueryBuilder.prototype.select = function (cols) {
        this.columns = [];
        for (var index in cols) {
            var col = cols[index];
            this.columns.push("`" + col + "`");
        }
    };
    QueryBuilder.prototype.from = function (table) {
        this.table = table;
    };
    QueryBuilder.prototype.where = function (column, comparator, value) {
        var actualComparator = this.comparators[comparator];
        var whereString = " `" + column + "` " + actualComparator + " '" + value + "'";
        this.conditions.push(whereString);
    };
    QueryBuilder.prototype.orderBy = function (column, direction) {
        this.orderByColumn = column;
        this.orderDirection = direction;
    };
    QueryBuilder.prototype.getSelect = function () {
        var cols = this.columns.join(',');
        var query = [];
        query.push("SELECT * FROM `" + this.table + "`");
        if (this.conditions.length > 0) {
            query.push("WHERE");
            this.conditions.forEach(function (value) { return query.push(value); });
        }
        if (this.orderByColumn && this.orderDirection) {
            query.push("ORDER BY " + this.orderByColumn + " " + this.orderDirection);
        }
        return query.join(" ");
    };
    QueryBuilder.prototype.getDelete = function () {
        return "DELETE FROM...";
    };
    QueryBuilder.prototype.getUpdate = function (values) {
        return "UPDATE ... SET x=x, y=y ..";
    };
    return QueryBuilder;
}());
exports.QueryBuilder = QueryBuilder;
