import * as mysql from 'mysql';

export class Connection{
    private pool;
    constructor(){
        this.pool = mysql.createPool({
            host:       '127.0.0.1',
            port:       9002,
            user:       'stocks',
            password:   'stock1234',
            database:   'stocks'
        });
    }
    
    runQuery(query, callback){
        this.getConnection(
            function(err, conn){
                if(err){
                    console.log(err);
                }else{
                 conn.query(query, callback) ;
                }
            }

        );
    }

    getConnection(callback){
        this.pool.getConnection(callback);

    }    
}

