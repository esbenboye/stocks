import * as express from 'express';
import {StockValueHttpController} from '../Controllers/StockValueHttpController';

let stockValueHttpController:StockValueHttpController = new StockValueHttpController();
let bodyParser = require('body-parser');

let app = express();
app.use(bodyParser.json());
const port:number = 8080;

app.post('/stockvalue',(req, res) => stockValueHttpController.create(req, res));
app.get('/stockvalue/browse', (req, res) => stockValueHttpController.browse(req, res));

app.listen(port, () => console.log(`Listening to port ${port}!`));
