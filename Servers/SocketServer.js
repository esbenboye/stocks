"use strict";
exports.__esModule = true;
var StockValueSocketController_1 = require("../Controllers/StockValueSocketController");
var ioClient = require('socket.io-client');
var url = 'https://ws-api.iextrading.com/1.0/deep';
var socket = ioClient(url);
var stockValueSocketController = new StockValueSocketController_1.StockValueSocketController();
socket.on('message', function (message) { return stockValueSocketController.create(message); });
/*
Not working as intended
When looking at the socket example (https://iextrading.com/developer/docs/#official-price), the code seems to be in order.

When looking at the http request example, then it also return an empty response (e.g. https://api.iextrading.com/1.0/deep/official-price?symbols=snap), so I'm not sure what's goind on.

I've made a andpoint that will allow me to test the code anyway. Please check the readme and the postman collection.

*/
socket.on('connect', function () {
    socket.emit('subscribe', JSON.stringify({
        symbols: ['snap', 'fb', 'nke', 'yum', 'lee'],
        channels: ['officialprice']
    }));
});
