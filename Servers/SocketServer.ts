import {StockValueSocketController} from '../Controllers/StockValueSocketController';
const ioClient = require('socket.io-client');

let url = 'https://ws-api.iextrading.com/1.0/deep'
let socket = ioClient(url);
let stockValueSocketController:StockValueSocketController = new StockValueSocketController();

socket.on('message', message => stockValueSocketController.create(message));

/*
Not working as intended
When looking at the socket example (https://iextrading.com/developer/docs/#official-price), the code seems to be in order.

When looking at the http request example, then it also return an empty response (e.g. https://api.iextrading.com/1.0/deep/official-price?symbols=snap), so I'm not sure what's goind on.

I've made a andpoint that will allow me to test the code anyway. Please check the readme and the postman collection.

*/

socket.on('connect', () => {
  socket.emit('subscribe', JSON.stringify({
    symbols: ['snap','fb','nke','yum','lee'],
    channels: ['officialprice'],
  }))
})