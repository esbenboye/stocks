"use strict";
exports.__esModule = true;
var StockValueService_1 = require("../Services/StockValueService");
var StockValueSocketController = /** @class */ (function () {
    function StockValueSocketController() {
        this.stockValueService = new StockValueService_1.StockValueService();
    }
    StockValueSocketController.prototype.create = function (message) {
        var obj = JSON.parse(message);
        this.stockValueService.create(obj.symbol, obj.data.price, function (error, result) { return console.log("Incoming socket saved"); });
    };
    return StockValueSocketController;
}());
exports.StockValueSocketController = StockValueSocketController;
