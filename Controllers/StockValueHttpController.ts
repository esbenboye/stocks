import {StockValueService} from '../Services/StockValueService';

export class StockValueHttpController{

    private stockValueService:StockValueService;

    constructor(){
        this.stockValueService = new StockValueService();
    }

    create(request, response){
        let symbol:string = request.body.symbol;
        let price:number = request.body.data.price;
        this.stockValueService.create(
            symbol, 
            price,
            (error, result) => response.send(result)
        );
    }

    browse(request, response){
        this.stockValueService.browse(request, response, (error, result) => response.send(result));
    }
}

