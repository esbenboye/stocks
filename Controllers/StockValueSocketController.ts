import {StockValueService} from '../Services/StockValueService';

export class StockValueSocketController{

    private stockValueService:StockValueService;

    constructor(){
        this.stockValueService = new StockValueService();
    }

    create(message){
        let obj = JSON.parse(message);

        this.stockValueService.create(
            obj.symbol, 
            obj.data.price,
            (error, result) => console.log("Incoming socket saved")
        );
    }
}

