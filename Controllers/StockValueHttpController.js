"use strict";
exports.__esModule = true;
var StockValueService_1 = require("../Services/StockValueService");
var StockValueHttpController = /** @class */ (function () {
    function StockValueHttpController() {
        this.stockValueService = new StockValueService_1.StockValueService();
    }
    StockValueHttpController.prototype.create = function (request, response) {
        var symbol = request.body.symbol;
        var price = request.body.data.price;
        this.stockValueService.create(symbol, price, function (error, result) { return response.send(result); });
    };
    StockValueHttpController.prototype.browse = function (request, response) {
        this.stockValueService.browse(request, response, function (error, result) { return response.send(result); });
    };
    return StockValueHttpController;
}());
exports.StockValueHttpController = StockValueHttpController;
